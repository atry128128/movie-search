import React from "react";
import MoviesList from "./MoviesList";
import {Form, Button} from 'react-bootstrap';
import styled from 'styled-components'

const TittleApp = styled.div`
    font-family: 'Potta One', cursive;
    font-size:100px;
    margin-bottom: 40px;
    color: #DC1A28;
        @media (max-width: 750px) {
            font-size:75px;
            margin-bottom: 12px;
        }
        @media (max-width: 510px) {
            font-size:60px;
        }    
`;

const InputButtonWrapper = styled.div`
    display: flex;
    justify-content: center;
`;

const FormControl = styled.div`
     width: 30vw !important;
        @media (max-width: 750px) {
            width: 50vw !important;
        }
        @media (max-width: 510px) {
            width:50vw !important;
            font-size: 13px !important;
        }
`;

const ButtonSC = styled(Button)`
    width: 10vw !important;
        @media (max-width: 750px) {
            width: 15vw !important;
        }
        @media (max-width: 510px) {
            width: 15vw !important;
            font-size: 13px !important;
        } 
`;

const LabelForm = styled.div`
    font-size: 20px;
    margin-bottom: 10px;
        @media (max-width: 750px) {
            font-size: 18px;
        }
        @media (max-width: 510px) {
            font-size: 14px;
        }
`;

const HomePage = () => {
    const [inputValue, setImputValue] = React.useState("");
    const [inputValueProps, setInputValueProps] = React.useState("");


    const handleSubmit = (e) => {
        e.preventDefault();
        setInputValueProps(inputValue);
    }

    return (
        <>
            <TittleApp>JetFlix</TittleApp>
            <Form onSubmit={handleSubmit}>
                <Form.Group className={'form'}>
                    <LabelForm>
                        What movie are you looking for?
                    </LabelForm>
                    <InputButtonWrapper>
                        <FormControl>
                            <Form.Control type="text"
                                          onChange={e => setImputValue(e.target.value)}
                                          placeholder="Type the word(s)..."/>
                        </FormControl>
                        <ButtonSC variant="primary" type="submit">
                            Search
                        </ButtonSC>
                    </InputButtonWrapper>
                    <Form.Text className="text-muted">
                        We have ~1,000,000,000 movies!
                    </Form.Text>
                </Form.Group>
            </Form>
            <MoviesList inputValue={inputValueProps}/>
        </>
    )
}
export default HomePage;