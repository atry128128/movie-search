import {getPreviousNumber, isNumberValid} from "../funtions";

test('test isNumberValid', () => {
    expect(isNumberValid(0, 100)).toBe(false);
    expect(isNumberValid(-1, 100)).toBe(false);
    expect(isNumberValid(101, 100)).toBe(false);
    expect(isNumberValid(99, 100)).toBe(true);
    expect(isNumberValid(1, 100)).toBe(true);
});

test('test getPreviousNumber', () => {
    expect(getPreviousNumber(10, 100)).toBe(9);
    expect(getPreviousNumber(1, 100)).toBe(0);
    expect(getPreviousNumber(100, 102)).toBe(99);
});

