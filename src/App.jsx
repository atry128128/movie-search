import React from "react";
import styled from 'styled-components'
import HomePage from "./components/HomePage";

const AppContainer = styled.div`
    margin: 30px;
    text-align: center;
        @media (max-width: 750px) {
            margin: 15px;
        }
`;

function App() {
    return (
        <AppContainer>
            <HomePage/>
        </AppContainer>
    );
}

export default App;
