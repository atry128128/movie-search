import Pagination from "react-bootstrap/Pagination";
import {getPreviousNumber, isNumberValid} from "../funtions";
import styled from 'styled-components';

const PaginationContainer = styled.div`
    margin-bottom: 50px;
        @media (max-width: 750px) {
            font-size: 15px;
        }
        @media (max-width: 510px) {
            font-size: 10px;
        }
`;

const PaginationComponent = ({active, setActive, pagesAmount}) => {
    let items = [];

    for (let number = active - 2; number <= active + 2; number++) {
        if (isNumberValid(number, pagesAmount)) {
            items.push(
                <Pagination.Item
                    onClick={() => setActive(number)}
                    key={number}
                    active={number === active}
                >
                    {number}
                </Pagination.Item>,
            )
        }
    }

    return (
        <PaginationContainer>
            <Pagination>
                <Pagination.First onClick={() => setActive(1)}/>
                <Pagination.Prev onClick={() => active > 1 && setActive((currentPage) => {
                    return getPreviousNumber(currentPage, pagesAmount);
                })}/>
                {items}
                <Pagination.Next onClick={() => setActive((a) => {
                    return a < pagesAmount - 1 ? a + 1 : a;
                })}/>
                <Pagination.Last onClick={() => active < pagesAmount - 1 && setActive(pagesAmount - 1)}/>
            </Pagination>
        </PaginationContainer>
    )
}
export default PaginationComponent;
