import React from "react";
import styled from 'styled-components';

const MovieContainer = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 60vw;
    color: black;
    background-color: #F48B8B;
    margin: 5px;
    padding: 5px;
    border-radius: 5px;
        @media (max-width: 750px) {
            font-size: 15px !important;
            width: 85vw;
        }
        @media (max-width: 510px) {
            font-size: 10px !important;
        }
`;

const TitleMovie = styled.div`
    width: 60%;
`;

const Movie = (props) => {

    return (
        <>
            <MovieContainer >
                <TitleMovie >{props.title}</TitleMovie>
                <div>{props.year}</div>
            </MovieContainer>
        </>
    )
}
export default Movie;