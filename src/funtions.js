export function isNumberValid(number, pagesAmount) {
    return number > 0 && number < pagesAmount;
}

export function getPreviousNumber(currentPage, pagesAmount) {
    if (currentPage > 0 && currentPage < pagesAmount)
        return currentPage < pagesAmount - 1 ? currentPage - 1 : currentPage;
    else if (!(currentPage < pagesAmount)) return pagesAmount;
    else return 0;
}