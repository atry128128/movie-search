import React from "react";
import Pagination from "./Pagination";
import Movie from "./Movie";
import styled from 'styled-components'

const WrapperMovies = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 35px;
`;

const WrapperPagination = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 7px;
`;

const NoResult = styled.div`
    margin-top: 150px;
    font-size: 25px;
`;

const MoviesList = (props) => {
    const API_KEY = 'de396958';
    const URL_API = `https://www.omdbapi.com/?s=${props.inputValue}&apikey=${API_KEY}&page=`;

    const [resultsAmount, setResultsAmount] = React.useState(null);
    const [active, setActive] = React.useState(1);
    const [moviesInPage, setMoviesInPage] = React.useState([]);
    const [isLoadingData, setIsLoadingData] = React.useState(false);

    React.useEffect(() => {
        fetch(URL_API + '1')
            .then(response => (response.json()))
            .then(data => {
                setResultsAmount(data.totalResults);
                setIsLoadingData(true);
            })
    }, [URL_API]);

    React.useEffect(() => {
        if (resultsAmount > 0) {
            fetch(URL_API + (active + 1).toString())
                .then(response => (response.json()))
                .then(data => {
                        if (data.Search) setMoviesInPage(() => ([...data.Search]))
                    }
                );
        }
    }, [resultsAmount, active, URL_API]);


    return (
        <>
            {resultsAmount ?
                <WrapperMovies>
                    <div>
                        {moviesInPage && moviesInPage.map((move, key) =>
                            <Movie
                                key={key}
                                year={move.Year}
                                image={move.Poster}
                                title={move.Title}
                            />)}
                    </div>
                </WrapperMovies> : (isLoadingData && props.inputValue) && <NoResult>no results</NoResult>}
            <WrapperPagination>
                {resultsAmount &&
                <Pagination
                    setActive={setActive}
                    active={active}
                    pagesAmount={Math.floor(resultsAmount / 10)}
                />}
            </WrapperPagination>
        </>
    );
}

export default MoviesList;
